import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { Furniture } from './models';

@Injectable({
  providedIn: 'root'
})
export class FurntureService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Furniture[]> {
    return this.http.get<Furniture[]>('/api/furniture/GetAll');
  }

  search(text: string): Observable<Furniture[]> {
    return this.http.get<Furniture[]>(`/api/furniture/Search/${text}`);
  }

  addFurniture(title: string, description: string, typeOfFurniture: string,
    price: number, material: string): Observable<string> {
    const model = { title, description, typeOfFurniture, price, material};
    return this.http.post<string>('/api/furniture/AddFurniture', model);
  }

  deleteFurniture(id: Guid): Observable<Furniture> {
    return this.http.delete<Furniture>(`/api/furniture/DeleteById/${id}`);
  }

  getFurnitureById(furnitureId: Guid): Observable<Furniture> {
    return this.http.get<Furniture>(`/api/furniture/GetById/${furnitureId}`);
  }

  editFurniture(furnitureId: Guid, title: string, description: string, typeOfFurniture: string,
    price: number, material: string): Observable<string> {
    const model = {
      furnitureId: furnitureId.toString(), title, description, typeOfFurniture,
      price, material
    };
    return this.http.put<string>('/api/furniture/UpdateFurniture', model);
  }
}
