import { Component, OnInit } from '@angular/core';
import { Guid } from 'guid-typescript';
import { FurntureService } from '../furnture.service';
import { Furniture } from '../models';
import { NzTableModule } from 'ng-zorro-antd/table';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.less']
})
export class WelcomeComponent implements OnInit {

  constructor(private furnitureService: FurntureService) { }
  editCache: { [key: string]: { edit: boolean; data: Furniture } } = {};
  listOfData: Furniture[] = [];
  isVisible = false;
  isVisibleEdit = false;
  newMaterial: string = '';
  newPrice: number = 0;
  newTypeOfFurniture: string = '';
  newDescription: string = '';
  newTitle: string = '';

  items: Furniture[] = [];

  ngOnInit() {
    this.init();
  }

  init() {
    this.furnitureService.search(this.searchValue).subscribe((data) => {
      this.items = data;
      this.listOfData = data
      this.updateEditCache();
    });
  }

  startEdit(id: Guid): void {
    this.editCache[id.toString()].edit = true;
  }

  cancelEdit(id: Guid): void {
    const index = this.listOfData.findIndex(item => item.id === id);
    this.editCache[id.toString()] = {
      data: { ...this.listOfData[index] },
      edit: false
    };
  }

  saveEdit(id: Guid): void {
    const index = this.listOfData.findIndex(item => item.id === id);
    const item = this.editCache[id.toString()].data;
    this.furnitureService.editFurniture(item.id, item.title, item.description,
      item.typeOfFurniture, item.price, item.material).subscribe(() => {
        Object.assign(this.listOfData[index], this.editCache[id.toString()].data);
        this.editCache[id.toString()].edit = false;
      });
  }

  updateEditCache(): void {
    this.listOfData.forEach(item => {
      this.editCache[item.id.toString()] = {
        edit: false,
        data: { ...item }
      };
    });
  }

  addFurniture(title: string, description: string, typeOfFurniture: string,
    price: number, material: string) {
    this.furnitureService.addFurniture(title, description, typeOfFurniture, price, material)
      .subscribe(() => {
        this.init();
      });
    this.isVisible = false;
  }

  deleteFurniture(id: Guid) {
    this.furnitureService.deleteFurniture(id).subscribe(() => {
      this.init();
    });
  }

  editFurniture(furnitureId: Guid, title: string, description: string, typeOfFurniture: string,
    price: number, material: string) {
    this.furnitureService
      .editFurniture(furnitureId, title, description, typeOfFurniture, price, material)
      .subscribe(() => {
        this.init();
      });
  }

  getNextId(): number {
    let max = 0;
    for (let i = 0; i < this.listOfData.length; i++) {
      if (+this.listOfData[i].id > max) {
        max = +this.listOfData[i].id;
      }
    }
    return max + 1;
  }

  deleteRow(id: string): void {
    this.listOfData = this.listOfData.filter(d => d.id.toString() !== id);
  }


  showModal(): void {
    this.isVisible = !this.isVisible;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  handleEditCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisibleEdit = false;
  }

  searchValue: string = '';

  reset(): void {
    this.searchValue = '';
    this.search();
  }

  search(): void {
    this.init();
    //this.listOfDisplayData = this.listOfData.filter((item: DataItem) => item.name.indexOf(this.searchValue) !== -1);
  }
}
