import { Guid } from 'guid-typescript';

export class Furniture {
  id!: Guid;
  title!: string;
  description!: string;
  typeOfFurniture!: string;
  price!: number;
  material!: string;
}
