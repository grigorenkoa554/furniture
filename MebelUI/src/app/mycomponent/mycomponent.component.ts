import { Component, OnInit } from '@angular/core';
import { Guid } from 'guid-typescript';
import { FurntureService } from '../furnture.service';
import { Furniture } from '../models';
import { NzTableModule } from 'ng-zorro-antd/table';

interface DataItem {
  name: string;
  age: number;
  address: string;
}

@Component({
  selector: 'app-mycomponent',
  templateUrl: './mycomponent.component.html',
  styleUrls: ['./mycomponent.component.less']
})
export class MycomponentComponent implements OnInit {

  constructor(private furnitureService: FurntureService) { }
 searchValue = '';
  visible = false;
  listOfData: DataItem[] = [
    {
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park'
    },
    {
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      name: 'Jim Red',
      age: 32,
      address: 'London No. 2 Lake Park'
    }
  ];
  listOfDisplayData = [...this.listOfData];

  reset(): void {
    this.searchValue = '';
    this.search();
  }

  search(): void {
    this.visible = false;
    this.listOfDisplayData = this.listOfData.filter((item: DataItem) => item.name.indexOf(this.searchValue) !== -1);
  }

  ngOnInit() {
  }
}
