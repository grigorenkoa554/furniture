﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Mebel_dot_net.Model
{
	[Table("Furniture")]
	public class Furniture
	{
		public Guid Id { get; set; }

		[Required]
		public string Title { get; set; }

		public string Description { get; set; }

		[Required]
		public double Price { get; set; }

		[Required]
		public string TypeOfFurniture { get; set; }
		public string Material { get; set; }
	}
}
