﻿using Mebel_dot_net.Model;
using Mebel_dot_net.Repository;
using Mebel_dot_net.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Mebel_dot_net.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class FurnitureController : Controller
	{
		private IFurnitureRepository furnRepository;
		private readonly IFurnitureService furnService;
		public FurnitureController(IFurnitureRepository furnRepository,
			IFurnitureService furnService)
		{
			this.furnRepository = furnRepository;
			this.furnService = furnService;
		}

		[HttpGet("[action]")]
		public IEnumerable<Furniture> GetAll()
		{
			using (furnRepository)
			{
				return furnRepository.GetAll();
			}
		}

		[HttpGet("[action]")]
		public IEnumerable<Furniture> GetFurnitureByDescription(string description)
		{
			return furnService.GetFurnitureByDescription(description);
		}

		[HttpGet("[action]")]
		public IEnumerable<Furniture> GetFurnitureByMaterial(string material)
		{
			return furnService.GetFurnitureByMaterial(material);
		}

		[HttpGet("[action]")]
		public IEnumerable<Furniture> GetFurnitureByPrice(double price)
		{
			return furnService.GetFurnitureByPrice(price);
		}

		[HttpGet("[action]")]
		public IEnumerable<Furniture> GetFurnitureByTitle(string title)
		{
			return furnService.GetFurnitureByTitle(title);
		}

		[HttpGet("[action]")]
		public IEnumerable<Furniture> GetFurnituresByTypeOfFurniture(string typeOfFurniture)
		{
			return furnService.GetFurnituresByTypeOfFurniture(typeOfFurniture);
		}

		[HttpGet("[action]")]
		[HttpGet("[action]/{str}")]
		public IEnumerable<Furniture> Search(string str)
		{
			return furnService.Search(str);
		}

		[HttpGet("[action]/{id}")]
		public Furniture GetById(Guid id)
		{
			using (furnRepository)
			{
				return furnRepository.GetById(id);
			}
		}

		[HttpPost("[action]")]
		public object AddFurniture(AddFurnitureModel model)
		{
			furnService.AddFurniture(model);
			return new { res = "Added." };
		}

		[HttpPut("[action]")]
		public object UpdateFurniture(UpdateFurnitureModel model)
		{
			furnService.UpdateFurniture(model);
			return new { res = "Updated." };
		}

		[HttpDelete("[action]/{id}")]
		public void DeleteById(Guid id)
		{
			using (furnRepository)
			{
				furnRepository.Delete(id);
			}
		}
	}
}
