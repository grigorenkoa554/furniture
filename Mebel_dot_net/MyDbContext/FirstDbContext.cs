﻿using Mebel_dot_net.Model;
using Microsoft.EntityFrameworkCore;

namespace Mebel_dot_net.MyDbContext
{
	public class FirstDbContext:DbContext
	{
        public FirstDbContext()
        {
            Database.EnsureCreated();
        }
        public DbSet<Furniture> Furnitures { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var cs = @"Data Source = 'DESKTOP-QP982B5'; 
                Initial Catalog='DB_Furniture';
                Integrated Security=true;";
            optionsBuilder.UseSqlServer(cs);
        }
    }
}
