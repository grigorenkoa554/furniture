﻿using Mebel_dot_net.Model;
using Mebel_dot_net.MyDbContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mebel_dot_net.Repository
{
    public interface IFurnitureRepository : IDisposable
    {
        void Add(Furniture item);
        List<Furniture> GetAll();
        Furniture GetById(Guid id);
        void Update(Furniture item);
        void Delete(Guid id);

    }
	public class FurnitureRepository : IFurnitureRepository
	{
		public void Add(Furniture item)
		{
			using(var dbContext = new FirstDbContext())
			{
				dbContext.Furnitures.Add(item);
				dbContext.SaveChanges();
			}
		}

		public void Delete(Guid id)
		{
			using (var dbContext = new FirstDbContext())
			{
				var item = dbContext.Furnitures.FirstOrDefault(x => x.Id == id);
				if (item == null)
				{
					throw new Exception("ID for delete does not exist");
				}
				dbContext.Furnitures.Remove(item);
				dbContext.SaveChanges();
			}
		}

		public void Dispose()
		{
			using (var dbContext = new FirstDbContext())
			{
				dbContext?.Dispose();
			}
		}

		public List<Furniture> GetAll()
		{
			using (var dbContext = new FirstDbContext())
			{
				var items = dbContext.Furnitures.OrderBy(x => x.Title).ToList();
				return items;
			}
		}

		public Furniture GetById(Guid id)
		{
			using (var dbContext = new FirstDbContext())
			{
				var item = dbContext.Furnitures.FirstOrDefault(x => x.Id == id);
				if (item == null)
				{
					throw new Exception("ID for getById does not exist");
				}
				return item;
			}
		}

		public void Update(Furniture item)
		{
			using (var dbContext = new FirstDbContext())
			{
				var old = dbContext.Furnitures.FirstOrDefault(x => x.Id == item.Id);
				if (old != null)
				{
					old.Description = item.Description;
					old.Material = item.Material;
					old.Price = item.Price;
					old.Title = item.Title;
					old.TypeOfFurniture = item.TypeOfFurniture;	
				}
				dbContext.SaveChanges();
			}
		}
	}
}
