﻿using Mebel_dot_net.Model;
using Mebel_dot_net.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Mebel_dot_net.Services
{
	public interface IFurnitureService
	{
		public List<Furniture> GetFurnitureByTitle(string title);
		public List<Furniture> GetFurnitureByDescription(string description);
		public List<Furniture> GetFurnituresByTypeOfFurniture(string typeOfFurniture);
		public List<Furniture> GetFurnitureByMaterial(string material);
		public List<Furniture> GetFurnitureByPrice(double price);
		public List<Furniture> Search(string str);
		public Furniture AddFurniture(AddFurnitureModel model);
		public object UpdateFurniture(UpdateFurnitureModel model);
	}

	public class FurnitureService : IFurnitureService
	{
		private readonly IFurnitureRepository repository;
		public FurnitureService(IFurnitureRepository repository)
		{
			this.repository = repository;
		}
		public List<Furniture> GetFurnitureByDescription(string description)
		{
			return repository.GetAll()
				.Where(x => x.Description.ToLower() == description.ToLower())
				.ToList();
		}

		public List<Furniture> GetFurnitureByMaterial(string material)
		{
			return repository.GetAll()
				.Where(x => x.Material.ToLower() == material.ToLower())
				.ToList();
		}

		public List<Furniture> GetFurnitureByPrice(double price)
		{
			return repository.GetAll()
				.Where(x => x.Price == price)
				.ToList();
		}

		public List<Furniture> GetFurnitureByTitle(string title)
		{
			return repository.GetAll()
				.Where(x => x.Title.ToLower() == title.ToLower())
				.ToList();
		}

		public List<Furniture> GetFurnituresByTypeOfFurniture(string typeOfFurniture)
		{
			return repository.GetAll()
				.Where(x => x.TypeOfFurniture.ToLower() == typeOfFurniture.ToLower())
				.ToList();
		}
		public Furniture AddFurniture(AddFurnitureModel model)
		{
			var furniture = new Furniture
			{
				Title = model.Title,
				Description = model.Description,
				Price = model.Price,
				TypeOfFurniture = model.TypeOfFurniture,
				Material = model.Material
			};
			var list = repository.GetAll();
			var count = 0;
			foreach (var item in list)
			{
				if (string.Equals(item.Title, furniture.Title,
					StringComparison.OrdinalIgnoreCase) &&
					string.Equals(item.Description, furniture.Description,
					StringComparison.OrdinalIgnoreCase) &&
					string.Equals(item.TypeOfFurniture, furniture.TypeOfFurniture,
					StringComparison.OrdinalIgnoreCase) &&
					string.Equals(item.Material, furniture.Material,
					StringComparison.OrdinalIgnoreCase) &&
					furniture.Price == item.Price)
				{
					count = -1;
				}
			}
			if (count == 0)
			{
				repository.Add(furniture);
			}
			return furniture;
		}

		public List<Furniture> Search(string str)
		{
			if (string.IsNullOrEmpty(str))
			{
				return repository.GetAll();
			}
			else
			{
				str = str.ToLower();
				var items = repository.GetAll()
					.Where(x => x.Description.ToLower().Contains(str) || x.Material.ToLower().Contains(str)
					|| x.Title.ToLower().Contains(str) || x.TypeOfFurniture.ToLower().Contains(str)
					|| Equals(x.Price, str.ToString())).ToList();
				return items;
			}
		}

		public object UpdateFurniture(UpdateFurnitureModel model)
		{
			var exists = repository
				.GetAll()
				.SingleOrDefault(x => x.Id == model.FurnitureId);
			if (exists == null)
			{
				throw new ArgumentException($"Furniture with id: {model.FurnitureId} not exists.");
			}
			exists.Title = model.Title;
			exists.Description = model.Description;
			exists.Price = model.Price;
			exists.TypeOfFurniture = model.TypeOfFurniture;
			exists.Material = model.Material;
			repository.Update(exists);

			return new { res = "Youre furniture is update!" };
		}
	}
	public class AddFurnitureModel
	{
		[Required]
		public string Title { get; set; }

		public string Description { get; set; }

		[Required]
		public double Price { get; set; }

		[Required]
		public string TypeOfFurniture { get; set; }
		public string Material { get; set; }
	}
	public class UpdateFurnitureModel
	{
		public Guid FurnitureId { get; set; }
		[Required]
		public string Title { get; set; }

		public string Description { get; set; }

		[Required]
		public double Price { get; set; }

		[Required]
		public string TypeOfFurniture { get; set; }
		public string Material { get; set; }
	}


}
